# AuditStore for Android

## Installation

### Gradle

Make sure Jitpack is available in `build.gradle`:

```gradle
allprojects {
    repositories {
	    // ...
	    maven { url 'https://jitpack.io' }
	}
}
```

From within the `app/build.gradle` file:

```gradle
dependencies {
    // ...
	implementation 'org.bitbucket.veritoll:auditstore-android:0.1.4'
}
```

## Configuration

You will need a few items to properly utilize **AuditStore for Android**:

 1. An API key
 2. A partner key
 3. A unique identity (at runtime)
 
For an example application please refer to the [AuditStore Android Example](https://bitbucket.org/veritoll/auditstore-android-example) app.
